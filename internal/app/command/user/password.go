package user

import (
	"context"
	"errors"
	"gitlab.com/ogroma6996/go-cqrs-boilerplate/internal/domain/user"
	"gitlab.com/ogroma6996/go-cqrs-boilerplate/internal/domain/user/phone"
)

type UpdatePasswordHandler struct {
	userRepo user.Repository
}

func NewUpdatePasswordHandler(userRepo user.Repository) UpdatePasswordHandler {
	return UpdatePasswordHandler{
		userRepo: userRepo,
	}
}

func (h UpdatePasswordHandler) Handle(ctx context.Context, phoneStr, oldPassword, newPassword string) error {
	ph, err := phone.NewPhone(phoneStr)
	if err != nil {
		return err
	}
	u, err := h.userRepo.GetUserByPhone(ctx, ph)
	if err != nil {
		return err
	}
	if !u.ComparePassword(oldPassword) {
		return errors.New("current password is incorrect")
	}
	err = user.WithPassword(newPassword)(u)
	if err != nil {
		return err
	}
	err = h.userRepo.UpdateUser(ctx, u)
	if err != nil {
		return err
	}

	return nil
}
