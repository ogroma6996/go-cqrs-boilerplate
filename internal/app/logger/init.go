package logger

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
)

// InitLogger inits logger
func InitLogger(logLevel string) error {
	logrus.SetOutput(os.Stdout)
	logrus.SetFormatter(&logrus.JSONFormatter{})
	lvl, err := logrus.ParseLevel(logLevel)
	if err != nil {
		return fmt.Errorf("failed to parse log level. %v", err)
	}
	logrus.SetLevel(lvl)

	return nil
}
