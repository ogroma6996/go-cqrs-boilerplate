package grpc

type GrpcServer struct {
	app *app.Application
	pb.UnimplementedBoilerplateServer
}

func NewGrpcServer(application *app.Application) GrpcServer {
	return GrpcServer{app: application}
}
