module gitlab.com/ogroma6996/go-cqrs-boilerplate

go 1.17

require (
	github.com/doug-martin/goqu/v9 v9.18.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/pressly/goose v2.7.0+incompatible
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	golang.org/x/crypto v0.0.0-20221012134737-56aed061732a // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
